FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["ITISApp/", "ITISAppBuild/"]
WORKDIR "/src/ITISAppBuild"
RUN dotnet restore 
COPY . .
RUN dotnet publish "ITISApp.Web/ITISApp.Web.csproj" -c release -o /app --no-restore

FROM node:12.16.1 AS node-build
WORKDIR /src
COPY ITISApp/ITISApp.Web/ClientApp .
RUN npm install --silent
RUN npm run build

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
COPY --from=build /app .
COPY --from=node-build /src/dist/spa ./ClientApp/dist/spa
ENTRYPOINT ["dotnet", "ITISApp.Web.dll"]