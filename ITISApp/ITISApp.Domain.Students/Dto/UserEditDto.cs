﻿namespace ITISApp.Domain.Students.Dto
{
    public class UserEditDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string MiddleName { get; set; }

        public string Email { get; set; }

        public string ImageUrl { get; set; }
    }
}