﻿using ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Students.Dto
{
    public class StudentEditDto : UserEditDto
    {
        public AcademicGroup AcademicGroup { get; set; }

        public int CourseNumber { get; set; }
    }
}