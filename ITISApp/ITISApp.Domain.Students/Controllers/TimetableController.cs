﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Extensions;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Students.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ITISApp.Domain.Students.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class TimetableController : Controller
    {
        private readonly IDataStore _dataStore;
        private readonly TimetableService _timetableService;
        private readonly ScheduleImportService _scheduleImportService;

        public TimetableController(
            IDataStore dataStore,
            TimetableService timetableService,
            ScheduleImportService scheduleImportService)
        {
            _dataStore = dataStore;
            _timetableService = timetableService;
            _scheduleImportService = scheduleImportService;
        }

        [HttpGet]
        public async Task<OperationResult> GetTimetable()
        {
            var login = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            var user = _dataStore.GetAll<User>().FirstOrDefault(x => x.Login == login);

            if (HttpContext.User.IsStudent())
            {
                var student = _dataStore.GetAll<Student>().FirstOrDefault(x => x.User.Id == user.Id);
                var lessonDtos = await _timetableService.GetStudentTimetable(student);
                return new OperationResult(true, lessonDtos);
            }

            if (HttpContext.User.IsTeacher())
            {
                var teacher = _dataStore.GetAll<Teacher>().FirstOrDefault(x => x.User.Id == user.Id);
                var lessonDtos = await _timetableService.GetTeacherTimetable(teacher);
                return new OperationResult(true, lessonDtos);
            }

            return new OperationResult(false, "Для текущего пользователя не найдена информация о расписании");
        }

        [HttpPost]
        public OperationResult GenerateTimetable(IFormCollection files)
        {
            var file = files.Files.FirstOrDefault();
            files.TryGetValue("dateStart", out var dateStart);
            files.TryGetValue("dateEnd", out var dateEnd);
            var fromDate = DateTime.Parse(dateStart);
            var toDate = DateTime.Parse(dateEnd);

            return _scheduleImportService.ImportSchedule(file.OpenReadStream(),
                fromDate.Date,
                toDate.Date);
        }
    }
}