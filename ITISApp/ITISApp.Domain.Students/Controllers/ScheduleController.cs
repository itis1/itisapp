﻿using System;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Students.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ITISApp.Domain.Students.Controllers
{
    // toDo контроллер для тестирования через Swagger, удалить после реализации функционала на клиенте.
    [Route("api/[controller]/[action]")]
    public class ScheduleController : Controller
    {
        private readonly ScheduleImportService _scheduleImportService;

        public ScheduleController(ScheduleImportService scheduleImportService)
        {
            _scheduleImportService = scheduleImportService;
        }

        [HttpPost]
        public OperationResult ImportSchedule([FromForm] IFormFile file, 
            DateTime fromDate, 
            DateTime toDate)
        {
            return _scheduleImportService.ImportSchedule(
                file.OpenReadStream(),
                fromDate,
                toDate);
        }
    }
}
