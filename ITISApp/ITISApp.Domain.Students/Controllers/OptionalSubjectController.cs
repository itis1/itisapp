﻿using System.Linq;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.LinkEntities;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Students.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.Domain.Students.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class OptionalSubjectController : Controller
    {
        private readonly IDataStore _dataStore;

        public OptionalSubjectController(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        [HttpGet]
        public async Task<OperationResult> GetOptionalSections(int studentId, int courseNumber)
        {
            var alreadySelectedSubjects = await _dataStore.GetAll<StudentGroup>()
                .Where(x => x.Student.Id == studentId)
                .Where(x => x.OptionalGroup.SubjectTeacher.Subject.OptionalCourseSection == null)
                .Select(x => new
                {
                    x.OptionalGroup.SubjectTeacher.Subject.Id,
                    SubjectName = x.OptionalGroup.SubjectTeacher.Subject.Name,
                    x.OptionalGroup.SubjectTeacher.Teacher.TeacherName,
                    x.OptionalGroup.SubGroupNumber
                })
                .ToListAsync();
            var subjectsDictionary = alreadySelectedSubjects
                .ToDictionary(x => x.Id);

            var alreadySelectedSections = await _dataStore.GetAll<StudentGroup>()
                .Where(x => x.Student.Id == studentId)
                .Where(x => x.OptionalGroup.SubjectTeacher.Subject.OptionalCourseSection != null)
                .Select(x => new
                {
                    x.OptionalGroup.SubjectTeacher.Subject.Id,
                    SectionId = x.OptionalGroup.SubjectTeacher.Subject.OptionalCourseSection.Id,
                    SubjectName = x.OptionalGroup.SubjectTeacher.Subject.Name,
                    x.OptionalGroup.SubjectTeacher.Teacher.TeacherName,
                    x.OptionalGroup.SubGroupNumber
                })
                .ToListAsync();
            var sectionsDictionary = alreadySelectedSections
                .ToDictionary(x => x.SectionId);

            var subjectsWithSubgroupDivision = await _dataStore.GetAll<TimetableInfo>()
                .Where(x => x.OptionalGroup != null)
                .Where(x => !x.SubjectTeacher.Subject.IsOptional)
                .Where(x => x.SubjectTeacher.Subject.CourseNumber == courseNumber)
                .Select(x => new OptionalCourseSelectionDto
                {
                    Name = x.SubjectTeacher.Subject.Name,
                    SubjectId = x.SubjectTeacher.Subject.Id,
                    HasSelectedGroup = subjectsDictionary.ContainsKey(x.SubjectTeacher.Subject.Id),
                    IsSubject = true,
                    SelectedCourseName = subjectsDictionary.ContainsKey(x.SubjectTeacher.Subject.Id)
                        ? subjectsDictionary[x.SubjectTeacher.Subject.Id].SubjectName
                        : null,
                    SelectedCourseTeacherName = subjectsDictionary.ContainsKey(x.SubjectTeacher.Subject.Id)
                        ? subjectsDictionary[x.SubjectTeacher.Subject.Id].TeacherName
                        : null,
                    SelectedSubgroupNumber = subjectsDictionary.ContainsKey(x.SubjectTeacher.Subject.Id)
                        ? subjectsDictionary[x.SubjectTeacher.Subject.Id].SubGroupNumber != null
                            ? $"гр №{subjectsDictionary[x.SubjectTeacher.Subject.Id].SubGroupNumber}"
                            : string.Empty
                        : null
                })
                .Distinct()
                .ToListAsync();

            var optionalSubjectSections = await _dataStore.GetAll<OptionalCourseSection>()
                .Where(x => x.CourseNumber == courseNumber)
                .Select(x => new OptionalCourseSelectionDto
                {
                    Name = x.Name,
                    SectionId = x.Id,
                    IsSubject = false,
                    HasSelectedGroup = sectionsDictionary.ContainsKey(x.Id),
                    SelectedCourseName = sectionsDictionary.ContainsKey(x.Id)
                        ? sectionsDictionary[x.Id].SubjectName
                        : null,
                    SelectedCourseTeacherName = sectionsDictionary.ContainsKey(x.Id)
                        ? sectionsDictionary[x.Id].TeacherName
                        : null,
                    SelectedSubgroupNumber = sectionsDictionary.ContainsKey(x.Id)
                        ? sectionsDictionary[x.Id].SubGroupNumber != null
                            ? $"гр №{sectionsDictionary[x.Id].SubGroupNumber}"
                            : string.Empty
                        : null
                })
                .ToListAsync();

            optionalSubjectSections.AddRange(subjectsWithSubgroupDivision);
            return new OperationResult
            {
                Success = true,
                Data = optionalSubjectSections
            };
        }

        [HttpGet]
        public async Task<OperationResult> GetSubjectsForSection(int sectionId)
        {
            var subjects = await _dataStore.GetAll<Subject>()
                .Where(x => x.IsOptional)
                .Where(x => x.OptionalCourseSection.Id == sectionId)
                .Select(x => new
                {
                    x.Id,
                    x.Name
                })
                .ToListAsync();

            return new OperationResult
            {
                Success = true,
                Data = subjects
            };
        }

        [HttpGet]
        public async Task<OperationResult> GetTeachersForSubject(int subjectId)
        {
            var teachers = await _dataStore.GetAll<TimetableInfo>()
                .Where(x => x.OptionalGroup != null)
                .Where(x => x.SubjectTeacher.Subject.Id == subjectId)
                .Select(x => new
                {
                    x.OptionalGroup.SubjectTeacher.Id,
                    x.OptionalGroup.SubjectTeacher.Teacher.TeacherName
                })
                .Distinct()
                .ToListAsync();

            return new OperationResult
            {
                Success = true,
                Data = teachers
            };
        }

        [HttpGet]
        public async Task<OperationResult> GetAvailableGroups(int subjectTeacherId, int academicGroupId)
        {
            var timetableInfos = (await _dataStore.GetAll<TimetableInfoGroup>()
                .Where(x => x.AcademicGroup.Id == academicGroupId)
                .Where(x => x.TimetableInfo.OptionalGroup != null)
                .Where(x => x.TimetableInfo.SubjectTeacher.Id == subjectTeacherId)
                .Select(x => new
                {
                    x.TimetableInfo.OptionalGroup.Id,
                    x.TimetableInfo.OptionalGroup.SubGroupNumber
                })
                .Distinct()
                .OrderBy(x => x.SubGroupNumber)
                .ToListAsync())
                .Select(x => new
                {
                    x.Id,
                    SubGroupNumber = x.SubGroupNumber != null
                        ? $"гр №{x.SubGroupNumber}"
                        : null
                })
                .ToList();

            return new OperationResult
            {
                Success = true,
                Data = timetableInfos
            };
        }

        [HttpPost]
        public async Task<OperationResult> AddStudentToOptionalGroup(
            int studentId,
            int optionalGroupId,
            [FromBody] OptionalCourseSelectionDto sectionDto)
        {
            var student = await _dataStore.GetAsync<Student>(studentId);
            var optionalGroup = await _dataStore.GetAsync<OptionalGroup>(optionalGroupId);
            var studentGroup = new StudentGroup
            {
                Student = student,
                OptionalGroup = optionalGroup
            };

            await CheckExistingStudentGroup(sectionDto, student);
            await _dataStore.CreateAndSaveAsync(studentGroup);

            return new OperationResult
            {
                Success = true
            };
        }

        private async Task CheckExistingStudentGroup(OptionalCourseSelectionDto sectionDto, Student student)
        {
            StudentGroup existingStudentGroup;
            if (sectionDto.IsSubject)
            {
                existingStudentGroup = await _dataStore.GetAll<StudentGroup>()
                    .Where(x => x.Student.Id == student.Id)
                    .FirstOrDefaultAsync(x => x.OptionalGroup.SubjectTeacher.Subject.Id == sectionDto.SubjectId);
            }
            else
            {
                existingStudentGroup = await _dataStore.GetAll<StudentGroup>()
                    .Where(x => x.Student.Id == student.Id)
                    .FirstOrDefaultAsync(x =>
                        x.OptionalGroup.SubjectTeacher.Subject.OptionalCourseSection.Id == sectionDto.SectionId);
            }

            if (existingStudentGroup != null)
            {
                await _dataStore.DeleteAndSaveAsync(existingStudentGroup);
            }
        }
    }
}