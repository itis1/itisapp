﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.LinkEntities;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Students.Dto;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.Domain.Students.Services
{
    public class TimetableService
    {
        private readonly IDataStore _dataStore;

        public TimetableService(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public async Task<List<LessonDto>> GetStudentTimetable(Student student)
        {
            var optionalGroupIds = await _dataStore.GetAll<StudentGroup>()
                .Where(x => x.Student.Id == student.Id)
                .Select(x => x.OptionalGroup.Id)
                .ToListAsync();

            var timetableInfoByAcademicGroupIds = await _dataStore.GetAll<TimetableInfoGroup>()
                .Where(x => x.AcademicGroup.Id == student.AcademicGroup.Id)
                .Select(x => x.TimetableInfo.Id)
                .ToListAsync();

            var timeTableInfoIds = await _dataStore.GetAll<TimetableInfo>()
                .Where(x => timetableInfoByAcademicGroupIds.Contains(x.Id))
                .Where(x => x.OptionalGroup != null && optionalGroupIds.Contains(x.OptionalGroup.Id)
                            || x.OptionalGroup == null)
                .Select(x => x.Id)
                .ToListAsync();

            return await GetLessonDtos(timeTableInfoIds);
        }

        public async Task<List<LessonDto>> GetTeacherTimetable(Teacher teacher)
        {
            var timetableInfoIds = await _dataStore.GetAll<TimetableInfo>()
                .Where(x => x.SubjectTeacher.Teacher.Id == teacher.Id)
                .Select(x => x.Id)
                .ToListAsync();
            return await GetLessonDtos(timetableInfoIds);
        }

        private async Task<List<LessonDto>> GetLessonDtos(List<int> timeTableInfoIds)
        {
            var timetableInfos = await _dataStore.GetAll<TimetableInfoGroup>()
                .Where(x => timeTableInfoIds.Contains(x.TimetableInfo.Id))
                .Select(x => new
                {
                    TimetableInfoId = x.TimetableInfo.Id,
                    GroupName = x.AcademicGroup.Name
                })
                .ToListAsync();
            var groupDictionary = timetableInfos
                .GroupBy(x => x.TimetableInfoId)
                .ToDictionary(x => x.Key,
                    x => string.Join(", ", x.Select(y => y.GroupName).OrderBy(z=> z)));

            var lessons = await _dataStore.GetAll<Lesson>()
                .Where(x => timeTableInfoIds.Contains(x.TimetableInfo.Id))
                .OrderBy(x => x.Time)
                .Select(lesson => new LessonDto
                {
                    Title = lesson.TimetableInfo.SubjectTeacher.Subject.Name,
                    RoomNumber = lesson.RoomNumber,
                    TeacherName = lesson.TimetableInfo.SubjectTeacher.Teacher.TeacherName,
                    Date = lesson.Time.Date.ToString("yyyy-MM-dd"),
                    Time = lesson.Time.ToString("HH:mm"),
                    Groups = groupDictionary.ContainsKey(lesson.TimetableInfo.Id)
                        ? groupDictionary[lesson.TimetableInfo.Id]
                        : null,
                    Duration = 90
                })
                .ToListAsync();

            return lessons;
        }
    }
}