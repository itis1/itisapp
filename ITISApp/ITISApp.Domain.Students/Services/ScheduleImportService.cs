﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ExcelDataReader;
using ITISApp.Domain.Core.Common;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Students.Models.LessonsGenerationModels;

namespace ITISApp.Domain.Students.Services
{
    /// <summary>
    /// Сервис импорта расписания.
    /// </summary>
    public class ScheduleImportService
    {
        private static readonly Regex LessonRegexPattern =
            new Regex(@"(((\w+\W*)+)\s-\s)(\w+\s\w\.(\w\.)?)\sв\s((\w+[^гр\.]\s?)+)(\s?гр\.(\d+))?(\s?\((.*)\))?",
                RegexOptions.Compiled,
                new TimeSpan(0, 0, 3));

        private static readonly Regex UnitedDataRegexPattern =
            new Regex(@"(\w+\s\w\.(\w\.)?)\sв\s((\w+[^гр\.]\s?)+)(\s?гр\.(\d+))?(\s?\((.*)\))?",
                RegexOptions.Compiled,
                new TimeSpan(0, 0, 3));

        private static readonly Regex LessonWithoutTeacherRegexPattern =
            new Regex(@"((\w+\W*)+)\sв\s((\w+[^гр\.]\s?)+)(\s?гр\.(\d+))?(\s?\((.*)\))?",
                RegexOptions.Compiled,
                new TimeSpan(0, 0, 3));

        private static readonly Regex AddInfoRegexPattern =
            new Regex(@"((\d+)н\.\s?)?(с\s(\d+)н\.(\sпо\s(\d+)н\.)?\s?)?(по\s(\w)\.н\.)?",
                RegexOptions.Compiled,
                new TimeSpan(0, 0, 2));

        private readonly LessonsGenerationService _lessonsGenerationService;

        public ScheduleImportService(LessonsGenerationService lessonsGenerationService)
        {
            _lessonsGenerationService = lessonsGenerationService;
        }

        private Dictionary<int, AcademicGroupModel> _academicGroupModelsDict;

        /// <summary>
        /// Словарь значений даты и времени первой пары
        /// в семестре в зависимости от даты начала семестра:
        /// ключ - индекс,
        /// значение - дата и время и признак состоится ли пара
        /// в 1 учебную неделю или будет со следующей.
        /// </summary>
        private Dictionary<int, Tuple<DateTime, bool>> _firstLessonDateTimesDict;

        private List<SubjectModel> _subjectModels;
        private List<TeacherModel> _teacherModels;
        private List<SubjectTeacherModel> _subjectTeacherModels;
        private List<OptionalGroupModel> _optionalGroupModels;
        private List<TimeTableLessonInfoGenerationModel> _timeTableLessonInfoGenerationModels;
        private List<LessonGenerationSettingsModel> _lessonGenerationSettingsModels;

        /// <summary>
        /// Метод для импорта расписания.
        /// </summary>
        /// <param name="fileContentStream"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public OperationResult ImportSchedule(
            Stream fileContentStream,
            DateTime fromDate,
            DateTime toDate)
        {
            try
            {
                using var reader = ExcelReaderFactory.CreateReader(fileContentStream);
                var result = reader.AsDataSet();
                var rows = result.Tables[0].Rows;
                var mergeCells = reader.MergeCells;

                InitData();
                FillAcademicGroupsInfo(rows);
                FillFirstLessonDatesInfo(reader.MergeCells, rows, fromDate);

                var totalColumnsCount = _academicGroupModelsDict.Max(x => x.Key);
                var totalRowsCount = _firstLessonDateTimesDict.Max(x => x.Key);

                for (var rowIndex = 3; rowIndex <= totalRowsCount; rowIndex++)
                {
                    for (int columnIndex = 2; columnIndex <= totalColumnsCount; columnIndex++)
                    {
                        var cellValue = rows[rowIndex][columnIndex].ToString();

                        if (!string.IsNullOrEmpty(cellValue))
                        {
                            var timeTableCurrentLessonInfoGenerationModels =
                                ProcessData(cellValue, rowIndex, columnIndex);

                            var mergeCellRange = mergeCells
                                .Where(x => x.FromRow == rowIndex)
                                .FirstOrDefault(x => x.FromColumn == columnIndex);

                            if (mergeCellRange != null)
                            {
                                FillForMergedCells(timeTableCurrentLessonInfoGenerationModels, mergeCellRange);
                            }
                        }
                    }
                }

                _lessonsGenerationService.GenerateLessons(
                    _timeTableLessonInfoGenerationModels,
                    fromDate,
                    toDate);
            }
            catch (ValidationException exception)
            {
                return new OperationResult(false, exception.Message);
            }

            return new OperationResult(true);
        }

        /// <summary>
        /// Метод для инициализации данных.
        /// </summary>
        private void InitData()
        {
            _academicGroupModelsDict = new Dictionary<int, AcademicGroupModel>();
            _timeTableLessonInfoGenerationModels = new List<TimeTableLessonInfoGenerationModel>();
            _subjectModels = new List<SubjectModel>();
            _teacherModels = new List<TeacherModel>();
            _subjectTeacherModels = new List<SubjectTeacherModel>();
            _optionalGroupModels = new List<OptionalGroupModel>();
            _firstLessonDateTimesDict = new Dictionary<int, Tuple<DateTime, bool>>();
            _lessonGenerationSettingsModels = new List<LessonGenerationSettingsModel>();
        }

        /// <summary>
        /// Метод для заполнения информации в объединенных ячейках.
        /// </summary>
        /// <param name="timeTableCurrentLessonInfoGenerationModels"></param>
        /// <param name="mergeCellRange"></param>
        private void FillForMergedCells(
            List<TimeTableLessonInfoGenerationModel> timeTableCurrentLessonInfoGenerationModels,
            CellRange mergeCellRange)
        {
            for (var columnIndex = mergeCellRange.FromColumn + 1; columnIndex <= mergeCellRange.ToColumn; columnIndex++)
            {
                var currentGroup = _academicGroupModelsDict[columnIndex];

                foreach (var model in timeTableCurrentLessonInfoGenerationModels)
                {
                    model.AcademicGroupModels.Add(currentGroup);
                }
            }
        }

        /// <summary>
        /// Метод для заполнения информации об академических группах.
        /// </summary>
        /// <param name="rows"></param>
        private void FillAcademicGroupsInfo(DataRowCollection rows)
        {
            var groupInfoRow = rows[2].ItemArray;

            for (var columnIndex = 2; columnIndex < groupInfoRow.Length; columnIndex++)
            {
                var groupNumber = groupInfoRow[columnIndex].ToString();

                if (string.IsNullOrEmpty(groupNumber))
                {
                    break;
                }

                _academicGroupModelsDict.Add(columnIndex, new AcademicGroupModel(groupNumber));
            }

            var courseNumber = 1;

            foreach (var group in _academicGroupModelsDict.GroupBy(x => int.Parse(x.Value.Name.Substring(3)) / 100))
            {
                foreach (var kvp in group)
                {
                    kvp.Value.CourseNumber = courseNumber;
                }

                courseNumber++;
            }
        }

        /// <summary>
        /// Метод для заполнения информации даты и времени первой пары в семестре
        /// на основе даты начала семестра.
        /// </summary>
        /// <param name="mergedCellRanges"></param>
        /// <param name="rows"></param>
        /// <param name="fromDate"></param>
        private void FillFirstLessonDatesInfo(
            CellRange[] mergedCellRanges,
            DataRowCollection rows,
            DateTime fromDate)
        {
            // Получаем общее количество пар в день из расписания.
            var timeTableLessonsInDayCount = mergedCellRanges
                .Where(x => x.FromRow == 3)
                .Where(x => x.FromColumn == 0)
                .Where(x => x.ToColumn == 0)
                .Select(x => x.ToRow - x.FromRow + 1)
                .Single();

            var lessonStartTimes = new List<TimeSpan>(timeTableLessonsInDayCount);

            var timeTableLessonsInfoFirstRowIndex = 3;
            var totalRowsCount = timeTableLessonsInfoFirstRowIndex + timeTableLessonsInDayCount;

            // Получаем времена начала пар.
            for (var rowIndex = timeTableLessonsInfoFirstRowIndex; rowIndex < totalRowsCount; rowIndex++)
            {
                var time = rows[rowIndex][1]
                    .ToString()
                    .Substring(0, 5)
                    .Split('.')
                    .Select(int.Parse)
                    .ToArray();

                lessonStartTimes.Add(new TimeSpan(time[0], time[1], 0));
            }

            // Для каждого дня недели получаем дату и время первого занятия
            // от указанной при импорте даты.
            for (var dayOfWeek = 1; dayOfWeek < 7; dayOfWeek++)
            {
                // Нахождение даты первой пары в семестре.
                // Пример - дата начала семестра 12 февраля 2020г., день недели - среда,
                // значит первая пара в понедельник состоится лишь 17 февраля.
                var firstLessonDate = fromDate
                    .AddDays((dayOfWeek - (int) fromDate.DayOfWeek + 7) % 7);

                // Если день недели указанный при импорте меньше
                // или равен дню недели в расписании первая пара состоится в 1 - нечетной неделе
                // иначе первая пара пропадает, и будет во 2 - нечетной неделе.
                var isFirstLessonNotMissing = fromDate.DayOfWeek <= firstLessonDate.DayOfWeek;

                foreach (var lessonStartTime in lessonStartTimes)
                {
                    _firstLessonDateTimesDict
                        .Add(
                            timeTableLessonsInfoFirstRowIndex,
                            new Tuple<DateTime, bool>(firstLessonDate.Add(lessonStartTime), isFirstLessonNotMissing));

                    timeTableLessonsInfoFirstRowIndex++;
                }
            }
        }

        /// <summary>
        /// Метод для обработки ячейки в таблице.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        private List<TimeTableLessonInfoGenerationModel> ProcessData(
            string value,
            int rowIndex,
            int columnIndex)
        {
            var timeTableCurrentLessonInfoGenerationModels = new List<TimeTableLessonInfoGenerationModel>();

            var severalValues = value.Split(" / ");

            foreach (var separatedValue in severalValues)
            {
                var splitByСolon = separatedValue.Split(": ");

                if (splitByСolon.Length > 1)
                {
                    if (splitByСolon[0].StartsWith("Курс по выбору"))
                    {
                        timeTableCurrentLessonInfoGenerationModels.AddRange(
                            ParseCourseData(
                                splitByСolon[1],
                                rowIndex,
                                columnIndex));
                    }
                    else
                    {
                        timeTableCurrentLessonInfoGenerationModels.AddRange(ParseUnitedData(
                            splitByСolon[0],
                            splitByСolon[1],
                            rowIndex,
                            columnIndex));
                    }
                }
                else
                {
                    timeTableCurrentLessonInfoGenerationModels.Add(ParseData(
                        separatedValue,
                        rowIndex,
                        columnIndex));
                }
            }

            return timeTableCurrentLessonInfoGenerationModels;
        }

        /// <summary>
        /// Метод для парсинга курса по выбору.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param> 
        private List<TimeTableLessonInfoGenerationModel> ParseCourseData(
            string value,
            int rowIndex,
            int columnIndex)
        {
            var timeTableCourseInfoGenerationModels = new List<TimeTableLessonInfoGenerationModel>();

            var severalValues = value.Split(", ");

            foreach (var separatedValue in severalValues)
            {
                var regexParsingModel = GetParsingModel(separatedValue, rowIndex, columnIndex, ParsingType.Normal);
                regexParsingModel.IsSubjectOptional = true;

                var currentGroup = _academicGroupModelsDict[columnIndex];

                var timeTableLessonInfoGenerationModel = GetTimeTableLessonInfoGenerationModel(
                    regexParsingModel,
                    currentGroup,
                    _firstLessonDateTimesDict[rowIndex],
                    true);

                timeTableCourseInfoGenerationModels.Add(timeTableLessonInfoGenerationModel);
            }

            return timeTableCourseInfoGenerationModels;
        }

        /// <summary>
        /// Метод для парсинга объединенных значений(например Английский язык).
        /// </summary>
        /// <param name="subjectName"></param>
        /// <param name="unitedDataValue"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        private List<TimeTableLessonInfoGenerationModel> ParseUnitedData(
            string subjectName,
            string unitedDataValue,
            int rowIndex,
            int columnIndex)
        {
            var timeTableCourseInfoGenerationModels = new List<TimeTableLessonInfoGenerationModel>();

            var severalValues = unitedDataValue.Split(", ");

            foreach (var separatedValue in severalValues)
            {
                var parsingModel = GetParsingModel(separatedValue, rowIndex, columnIndex, ParsingType.UnitedData);
                parsingModel.SubjectName = subjectName;

                var currentGroup = _academicGroupModelsDict[columnIndex];

                var timeTableLessonInfoGenerationModel = GetTimeTableLessonInfoGenerationModel(
                    parsingModel,
                    currentGroup,
                    _firstLessonDateTimesDict[rowIndex],
                    true);

                timeTableCourseInfoGenerationModels.Add(timeTableLessonInfoGenerationModel);
            }

            return timeTableCourseInfoGenerationModels;
        }

        /// <summary>
        /// Метод для парсинга обычной пары.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param> 
        private TimeTableLessonInfoGenerationModel ParseData(
            string value,
            int rowIndex,
            int columnIndex)
        {
            var parsingModel = GetParsingModel(
                value,
                rowIndex,
                columnIndex,
                value.StartsWith("Элективные курсы") 
                    ? ParsingType.LessonWithoutTeacher
                    : ParsingType.Normal);

            var currentGroup = _academicGroupModelsDict[columnIndex];

            return GetTimeTableLessonInfoGenerationModel(
                parsingModel,
                currentGroup,
                _firstLessonDateTimesDict[rowIndex],
                false);
        }

        /// <summary>
        /// Метод для получения парсинг модели по регулярному выражению.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <param name="parsingType"></param>
        /// <returns></returns>
        private ParsingModel GetParsingModel(
            string value,
            int rowIndex,
            int columnIndex,
            ParsingType parsingType)
        {
            try
            {
                return parsingType switch
                {
                    ParsingType.Normal => GetParsingModelByLessonPattern(value, rowIndex, columnIndex),
                    ParsingType.UnitedData => GetParsingModelByUnitedDataPattern(value, rowIndex, columnIndex),
                    ParsingType.LessonWithoutTeacher => GetParsingModelByLessonWithoutTeacherPattern(value, rowIndex, columnIndex)
                };
            }
            catch (RegexMatchTimeoutException)
            {
                throw new ValidationException(
                    $@"Не удалось обработать запись таблицы (превышено ожидание времени парсинга):
                             Значение - {value}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}.");
            }
        }

        private ParsingModel GetParsingModelByLessonPattern(string value, in int rowIndex, in int columnIndex)
        {
            var match = LessonRegexPattern.Match(value);
            if (!match.Success)
            {
                throw new ValidationException(
                    $@"Не удалось обработать запись таблицы (не соответствует шаблону):
                             Значение - {value}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}");
            }

            var separatedValues = match.Groups;

            return new ParsingModel
            {
                SubjectName = separatedValues[2].Value,
                TeacherName = separatedValues[4].Value,
                RoomNumber = separatedValues[6].Value.TrimEnd(),
                SubGroupNumber = !string.IsNullOrEmpty(separatedValues[8].Value)
                    ? int.Parse(separatedValues[9].Value)
                    : (int?)null,
                LessonGenerationSettingsModel = !string.IsNullOrEmpty(separatedValues[10].Value)
                    ? GetLessonsGetGenerationSettingsModel(separatedValues[11].Value, rowIndex, columnIndex)
                    : null
            };
        }

        private ParsingModel GetParsingModelByUnitedDataPattern(string value, in int rowIndex, in int columnIndex)
        {
            var match = UnitedDataRegexPattern.Match(value);
            if (!match.Success)
            {
                throw new ValidationException(
                    $@"Не удалось обработать запись таблицы (не соответствует шаблону):
                             Значение - {value}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}");
            }

            var separatedValues = match.Groups;

            return new ParsingModel
            {
                TeacherName = separatedValues[1].Value,
                RoomNumber = separatedValues[4].Value.TrimEnd(),
                SubGroupNumber = !string.IsNullOrEmpty(separatedValues[5].Value)
                    ? int.Parse(separatedValues[6].Value)
                    : (int?)null,
                LessonGenerationSettingsModel = !string.IsNullOrEmpty(separatedValues[7].Value)
                    ? GetLessonsGetGenerationSettingsModel(separatedValues[8].Value, rowIndex, columnIndex)
                    : null
            };
        }

        private ParsingModel GetParsingModelByLessonWithoutTeacherPattern(string value, in int rowIndex, in int columnIndex)
        {
            var match = LessonWithoutTeacherRegexPattern.Match(value);
            if (!match.Success)
            {
                throw new ValidationException(
                    $@"Не удалось обработать запись таблицы (не соответствует шаблону):
                             Значение - {value}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}");
            }

            var separatedValues = match.Groups;

            return new ParsingModel
            {
                SubjectName = separatedValues[1].Value,
                RoomNumber = separatedValues[3].Value.TrimEnd(),
                SubGroupNumber = !string.IsNullOrEmpty(separatedValues[5].Value)
                    ? int.Parse(separatedValues[6].Value)
                    : (int?)null,
                LessonGenerationSettingsModel = !string.IsNullOrEmpty(separatedValues[7].Value)
                    ? GetLessonsGetGenerationSettingsModel(separatedValues[8].Value, rowIndex, columnIndex)
                    : null
            };
        }

        /// <summary>
        /// Метод для создания модели генерации пары.
        /// </summary>
        /// <param name="parsingModel"></param>
        /// <param name="academicGroupModel"></param>
        /// <param name="firstLessonDateTime"></param>
        /// <param name="isNeedOptionalGroup"></param>
        /// <returns></returns>
        private TimeTableLessonInfoGenerationModel GetTimeTableLessonInfoGenerationModel(
            ParsingModel parsingModel,
            AcademicGroupModel academicGroupModel,
            Tuple<DateTime, bool> firstLessonDateTime,
            bool isNeedOptionalGroup)
        {
            var subjectTeacherModel = GetSubjectTeacherModel(
                academicGroupModel.CourseNumber,
                parsingModel.SubjectName,
                parsingModel.IsSubjectOptional,
                parsingModel.TeacherName);

            var optionalGroupModel = isNeedOptionalGroup
                ? GetOptionalGroupModel(subjectTeacherModel, parsingModel.SubGroupNumber)
                : null;

            var timeTableLessonInfoGenerationModel = _timeTableLessonInfoGenerationModels
                .Where(x => x.SubjectTeacherModel == subjectTeacherModel)
                .Where(x => x.OptionalGroupModel == optionalGroupModel)
                .Where(x => x.FirstLessonDateTime.Item1 == firstLessonDateTime.Item1)
                .Where(x => x.LessonGenerationSettingsModel == parsingModel.LessonGenerationSettingsModel)
                .FirstOrDefault(x => x.RoomNumber == parsingModel.RoomNumber);

            if (timeTableLessonInfoGenerationModel == null)
            {
                timeTableLessonInfoGenerationModel = new TimeTableLessonInfoGenerationModel(
                    subjectTeacherModel,
                    academicGroupModel,
                    optionalGroupModel,
                    parsingModel.LessonGenerationSettingsModel,
                    firstLessonDateTime,
                    parsingModel.RoomNumber);

                _timeTableLessonInfoGenerationModels.Add(timeTableLessonInfoGenerationModel);
            }
            else
            {
                timeTableLessonInfoGenerationModel.AcademicGroupModels.Add(academicGroupModel);
            }

            return timeTableLessonInfoGenerationModel;
        }

        /// <summary>
        /// Метод для получения связки предмет + учитель.
        /// </summary>
        /// <param name="courseNumber"></param>
        /// <param name="subjectName"></param>
        /// <param name="isOptional"></param>
        /// <param name="teacherName"></param>
        /// <returns></returns>
        private SubjectTeacherModel GetSubjectTeacherModel(
            int courseNumber,
            string subjectName,
            bool isOptional,
            string teacherName)
        {
            var subjectModel = GetSubjectModel(courseNumber, subjectName, isOptional);
            var teacherModel = GetTeacherModel(teacherName);

            var subjectTeacherModel = _subjectTeacherModels
                .Where(x => x.SubjectModel == subjectModel)
                .FirstOrDefault(x => x.TeacherModel == teacherModel);

            if (subjectTeacherModel == null)
            {
                subjectTeacherModel = new SubjectTeacherModel(subjectModel, teacherModel);
                _subjectTeacherModels.Add(subjectTeacherModel);
            }

            return subjectTeacherModel;
        }

        /// <summary>
        /// Метод для получения модели предмета.
        /// </summary>
        /// <param name="courseNumber"></param>
        /// <param name="subjectName"></param>
        /// <param name="isOptional"></param>
        /// <returns></returns>
        private SubjectModel GetSubjectModel(int courseNumber, string subjectName, bool isOptional)
        {
            var subjectModel = _subjectModels
                .Where(x => x.CourseNumber == courseNumber)
                .FirstOrDefault(x => x.Name == subjectName);

            if (subjectModel == null)
            {
                subjectModel = new SubjectModel(courseNumber, subjectName, isOptional);
                _subjectModels.Add(subjectModel);
            }

            return subjectModel;
        }

        /// <summary>
        /// Метод для получения модели учителя.
        /// </summary>
        /// <param name="teacherName"></param>
        /// <returns></returns>
        private TeacherModel GetTeacherModel(string teacherName)
        {
            if (string.IsNullOrEmpty(teacherName))
            {
                return null;
            }

            var teacherModel = _teacherModels
                .FirstOrDefault(x => x.Name == teacherName);

            if (teacherModel == null)
            {
                teacherModel = new TeacherModel(teacherName);
                _teacherModels.Add(teacherModel);
            }

            return teacherModel;
        }

        /// <summary>
        /// Метод для получения опциональной группы.
        /// </summary>
        /// <param name="subjectTeacherModel"></param>
        /// <param name="subGroupNumber"></param>
        /// <returns></returns>
        private OptionalGroupModel GetOptionalGroupModel(SubjectTeacherModel subjectTeacherModel, int? subGroupNumber)
        {
            var optionalGroupModel = _optionalGroupModels
                .Where(x => x.SubjectTeacherModel == subjectTeacherModel)
                .FirstOrDefault(x => x.SubGroupNumber == subGroupNumber);

            if (optionalGroupModel == null)
            {
                optionalGroupModel = new OptionalGroupModel(subjectTeacherModel, subGroupNumber);
                _optionalGroupModels.Add(optionalGroupModel);
            }

            return optionalGroupModel;
        }

        private LessonGenerationSettingsModel GetLessonsGetGenerationSettingsModel(
            string addInfo,
            int rowIndex,
            int columnIndex)
        {
            var match = AddInfoRegexPattern.Match(addInfo);
            if (!match.Success)
            {
                throw new ValidationException(
                    $@"Не удалось обработать дополнительную информацию (не соответствует шаблону):
                             Значение - {addInfo}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}");
            }

            var separatedValues = match.Groups;

            bool? isOddOrEvenWeeksOnly = null;
            int? fromWeek = null;
            int? toWeek = null;
            int? lessonsCount = null;

            if (!string.IsNullOrEmpty(separatedValues[3].Value))
            {
                fromWeek = int.Parse(separatedValues[4].Value);
                if (!string.IsNullOrEmpty(separatedValues[5].Value))
                {
                    toWeek = int.Parse(separatedValues[6].Value);
                }
            }
            else if (!string.IsNullOrEmpty(separatedValues[1].Value))
            {
                lessonsCount = int.Parse(separatedValues[2].Value);
            }

            if (!string.IsNullOrEmpty(separatedValues[7].Value))
            {
                isOddOrEvenWeeksOnly = separatedValues[8].Value == "н";
            }

            if (fromWeek == null
                && toWeek == null 
                && lessonsCount == null
                && isOddOrEvenWeeksOnly == null)
            {
                throw new ValidationException(
                    $@"Не удалось обработать дополнительную информацию (не соответствует шаблону):
                             Значение - {addInfo}
                             Строка - {rowIndex}
                             Колонка - {columnIndex}");
            }

            var settings = _lessonGenerationSettingsModels
                .Where(x => x.FromWeek == fromWeek)
                .Where(x => x.ToWeek == toWeek)
                .Where(x => x.IsOddOrEvenWeeksOnly == isOddOrEvenWeeksOnly)
                .FirstOrDefault(x => x.LessonsCount == lessonsCount);

            if (settings == null)
            {
                settings = new LessonGenerationSettingsModel()
                {
                    FromWeek = fromWeek,
                    ToWeek = toWeek,
                    IsOddOrEvenWeeksOnly = isOddOrEvenWeeksOnly,
                    LessonsCount = lessonsCount
                };

                _lessonGenerationSettingsModels.Add(settings);
            }

            return settings;
        }
    }
}
