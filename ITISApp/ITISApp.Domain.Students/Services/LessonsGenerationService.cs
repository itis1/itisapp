﻿using ITISApp.Domain.Students.Models.LessonsGenerationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.LinkEntities;
using ITISApp.Domain.Core.Interfaces;

namespace ITISApp.Domain.Students.Services
{
    /// <summary>
    /// Сервис для генерации пар.
    /// </summary>
    public class LessonsGenerationService
    {
        private readonly IDataStore _dataStore;

        public LessonsGenerationService(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        private List<Subject> _existSubjects;
        private List<Teacher> _existTeachers;
        private List<SubjectTeacher> _existSubjectTeachers;
        private List<AcademicGroup> _existAcademicGroups;
        private List<OptionalGroup> _existOptionalGroups;

        /// <summary>
        /// Метод для генерации пар.
        /// </summary>
        /// <param name="timeTableLessonInfoGenerationModels"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        public void GenerateLessons(
            List<TimeTableLessonInfoGenerationModel> timeTableLessonInfoGenerationModels,
            DateTime fromDate,
            DateTime toDate)
        {
            FillExistData();
            
            _dataStore.InTransaction(() =>
            {
                var semester = new Semester
                {
                    DateStart = fromDate,
                    DateEnd = toDate
                };

                _dataStore.Create(semester);

                foreach (var timeTableLessonInfoGenerationModel in timeTableLessonInfoGenerationModels)
                {
                    var timeTableInfo = new TimetableInfo
                    {
                        Semester = semester,
                        SubjectTeacher = GetSubjectTeacher(timeTableLessonInfoGenerationModel.SubjectTeacherModel),
                        RoomNumber = timeTableLessonInfoGenerationModel.RoomNumber,
                        DayOfWeek = timeTableLessonInfoGenerationModel.FirstLessonDateTime.Item1.DayOfWeek,
                        Time = timeTableLessonInfoGenerationModel.FirstLessonDateTime.Item1.TimeOfDay
                    };

                    if (timeTableLessonInfoGenerationModel.OptionalGroupModel != null)
                    {
                        timeTableInfo.OptionalGroup = GetOptionalGroup(
                            timeTableInfo.SubjectTeacher, 
                            timeTableLessonInfoGenerationModel.OptionalGroupModel.SubGroupNumber);
                    }

                    _dataStore.Create(timeTableInfo);

                    foreach (var academicGroup in timeTableLessonInfoGenerationModel.AcademicGroupModels)
                    {
                        var timeTableInfoGroup = new TimetableInfoGroup
                        {
                            TimetableInfo = timeTableInfo,
                            AcademicGroup = GetAcademicGroup(academicGroup)
                        };

                        _dataStore.Create(timeTableInfoGroup);
                    }

                    GenerateLessonsByTimeTableInfo(toDate, timeTableLessonInfoGenerationModel, timeTableInfo);
                }
            });
            
        }
        
        /// <summary>
        /// Генерация пар по конкретной записи в расписании.
        /// </summary>
        /// <param name="toDate"></param>
        /// <param name="timeTableLessonInfoGenerationModel"></param>
        /// <param name="timeTableInfo"></param>
        private void GenerateLessonsByTimeTableInfo(
            DateTime toDate, 
            TimeTableLessonInfoGenerationModel timeTableLessonInfoGenerationModel,
            TimetableInfo timeTableInfo)
        {
            var currentDateTime = timeTableLessonInfoGenerationModel.FirstLessonDateTime.Item1;

            var lastDate = toDate;
            var daysToAdd = 7;

            var lessonGenerationSettingsModel = timeTableLessonInfoGenerationModel.LessonGenerationSettingsModel;

            if (lessonGenerationSettingsModel != null)
            {
                if (lessonGenerationSettingsModel.FromWeek.HasValue)
                {
                    var fromWeek = lessonGenerationSettingsModel.FromWeek.Value;

                    if (fromWeek > 1)
                    {
                        currentDateTime = currentDateTime.AddDays(
                            timeTableLessonInfoGenerationModel.FirstLessonDateTime.Item2
                                ? (fromWeek - 1) * 7
                                : (fromWeek - 2) * 7);
                    }

                    if (lessonGenerationSettingsModel.ToWeek.HasValue)
                    {
                        lastDate = currentDateTime.AddDays(
                            (lessonGenerationSettingsModel.ToWeek.Value - fromWeek) * 7);
                    }

                    if (lessonGenerationSettingsModel.IsOddOrEvenWeeksOnly.HasValue)
                    {
                        if ((fromWeek % 2 == 1) != lessonGenerationSettingsModel.IsOddOrEvenWeeksOnly.Value)
                        {
                            currentDateTime = currentDateTime.AddDays(7);
                        }
                    }
                }
                else
                {
                    if (lessonGenerationSettingsModel.IsOddOrEvenWeeksOnly.HasValue)
                    {
                        if (timeTableLessonInfoGenerationModel.FirstLessonDateTime.Item2 !=
                            lessonGenerationSettingsModel.IsOddOrEvenWeeksOnly)
                        {
                            currentDateTime = currentDateTime.AddDays(7);
                        }

                        daysToAdd = 14;
                    }

                    if (lessonGenerationSettingsModel.LessonsCount.HasValue)
                    {
                        lastDate = currentDateTime.AddDays(daysToAdd * (lessonGenerationSettingsModel.LessonsCount.Value - 1));
                    }
                }
            }

            while (currentDateTime.Date <= lastDate.Date)
            {
                var lesson = new Lesson
                {
                    TimetableInfo = timeTableInfo,
                    RoomNumber = timeTableInfo.RoomNumber,
                    Time = currentDateTime
                };

                _dataStore.Create(lesson);

                currentDateTime = currentDateTime.AddDays(daysToAdd);
            }
        }

        /// <summary>
        /// Метод для получения(создания при отсутствии) академической подгруппы.
        /// </summary>
        /// <param name="academicGroupModel"></param>
        /// <returns></returns>
        private AcademicGroup GetAcademicGroup(AcademicGroupModel academicGroupModel)
        {
            var academicGroup = _existAcademicGroups
                .Where(x => x.CourseNumber == academicGroupModel.CourseNumber)
                .FirstOrDefault(x => x.Name == academicGroupModel.Name);

            if (academicGroup == null)
            {
                academicGroup = new AcademicGroup
                {
                    CourseNumber = academicGroupModel.CourseNumber,
                    Name = academicGroupModel.Name
                };

                _dataStore.Create(academicGroup);
                _existAcademicGroups.Add(academicGroup);
            }

            return academicGroup;
        }

        /// <summary>
        /// Метод для получения(создания при отсутствии) опциональной подгруппы.
        /// </summary>
        /// <param name="subjectTeacher"></param>
        /// <param name="subGroupNumber"></param>
        /// <returns></returns>
        private OptionalGroup GetOptionalGroup(SubjectTeacher subjectTeacher, int? subGroupNumber)
        {
            var optionalGroup = _existOptionalGroups
                .Where(x => x.SubGroupNumber == subGroupNumber)
                .FirstOrDefault(x => x.SubjectTeacher == subjectTeacher);

            if (optionalGroup == null)
            {
                optionalGroup = new OptionalGroup
                {
                    SubjectTeacher = subjectTeacher,
                    SubGroupNumber = subGroupNumber
                };

                _dataStore.Create(optionalGroup);
                _existOptionalGroups.Add(optionalGroup);
            }

            return optionalGroup;
        }

        /// <summary>
        /// Метод для получения(создания при отсутствии) сущности предмет + учитель.
        /// </summary>
        /// <param name="subjectTeacherModel"></param>
        /// <returns></returns>
        private SubjectTeacher GetSubjectTeacher(SubjectTeacherModel subjectTeacherModel)
        {
            var subject = GetSubject(subjectTeacherModel.SubjectModel);
            var teacher = GetTeacher(subjectTeacherModel.TeacherModel);

            var subjectTeacher = _existSubjectTeachers
                .Where(x => x.Subject == subject)
                .FirstOrDefault(x => x.Teacher == teacher);

            if (subjectTeacher == null)
            {
                subjectTeacher = new SubjectTeacher
                {
                    Subject = subject,
                    Teacher = teacher
                };

                _dataStore.Create(subjectTeacher);
                _existSubjectTeachers.Add(subjectTeacher);
            }

            return subjectTeacher;
        }

        /// <summary>
        /// Метод для получения(создания при отсутствии) сущности предмета.
        /// </summary>
        /// <param name="subjectModel"></param>
        /// <returns></returns>
        private Subject GetSubject(SubjectModel subjectModel)
        {
            var subject = _existSubjects
                .Where(x => x.IsOptional == subjectModel.IsOptional)
                .Where(x => x.CourseNumber == subjectModel.CourseNumber)
                .FirstOrDefault(x => x.Name == subjectModel.Name);

            if (subject == null)
            {
                subject = new Subject
                {
                    Name = subjectModel.Name,
                    CourseNumber = subjectModel.CourseNumber,
                    IsOptional = subjectModel.IsOptional
                };

                _dataStore.Create(subject);
                _existSubjects.Add(subject);
            }

            return subject;
        }

        /// <summary>
        /// Метод для получения(создания при отсутствии) сущности учителя.
        /// </summary>
        /// <param name="teacherModel"></param>
        /// <returns></returns>
        private Teacher GetTeacher(TeacherModel teacherModel)
        {
            if (teacherModel == null)
            {
                return null;
            }

            var teacher = _existTeachers
                .FirstOrDefault(x => x.TeacherName == teacherModel.Name);

            if (teacher == null)
            {
                teacher = new Teacher
                {
                    TeacherName = teacherModel.Name
                };

                _dataStore.Create(teacher);
                _existTeachers.Add(teacher);
            }

            return teacher;
        }

        /// <summary>
        /// Заполнение существующих данных.
        /// </summary>
        private void FillExistData()
        {
            _existSubjects = _dataStore.GetAll<Subject>()
                .ToList();

            _existTeachers = _dataStore.GetAll<Teacher>()
                .ToList();

            _existSubjectTeachers = _dataStore.GetAll<SubjectTeacher>()
                .ToList();

            _existAcademicGroups = _dataStore.GetAll<AcademicGroup>()
                .ToList();

            _existOptionalGroups = _dataStore.GetAll<OptionalGroup>()
                .ToList();
        }
    }
}
