﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ITISApp.DAL.Migrations
{
    public partial class OptionalCourseInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OptionalCourseSectionId",
                table: "Subjects",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OptionalCourseSection",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    CourseNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OptionalCourseSection", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_OptionalCourseSectionId",
                table: "Subjects",
                column: "OptionalCourseSectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_OptionalCourseSection_OptionalCourseSectionId",
                table: "Subjects",
                column: "OptionalCourseSectionId",
                principalTable: "OptionalCourseSection",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_OptionalCourseSection_OptionalCourseSectionId",
                table: "Subjects");

            migrationBuilder.DropTable(
                name: "OptionalCourseSection");

            migrationBuilder.DropIndex(
                name: "IX_Subjects_OptionalCourseSectionId",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "OptionalCourseSectionId",
                table: "Subjects");
        }
    }
}
