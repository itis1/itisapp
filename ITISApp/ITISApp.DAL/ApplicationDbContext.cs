﻿using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Entities.LinkEntities;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Semester> Semesters { get; set; }

        public DbSet<Student> Students { get; set; }
        public DbSet<AcademicGroup> AcademicGroups { get; set; }
        public DbSet<OptionalGroup> OptionalGroups { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Mark> Marks { get; set; }
        public DbSet<TimetableInfo> TimetableInfos { get; set; }
        public DbSet<TimetableInfoGroup> SubjectGroups { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<SubjectTeacher> SubjectTeachers { get; set; }
        public DbSet<FileInfo> FileInfos { get; set; }
        public DbSet<ResetPasswordToken> ResetPasswordTokens { get; set; }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}