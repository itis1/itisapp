﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using ITISApp.Domain.Core.Entities;
using vm = ITISApp.Domain.Teachers.Models;

namespace ITISApp.Domain.Teachers.Mapper
{
    /// <summary> Настройка маппера </summary>
    public class MappingProfile : Profile
    {
        /// <summary> Имя хоста </summary>
        private readonly string _hostName;

        public MappingProfile(vm.DomainOptions options)
        {
            _hostName = options?.HostName.EndsWith('/') ?? false ? options.HostName : $"{options?.HostName}/";

            CreateMap<FileInfo, vm.FileInfo>()
                .ForMember(x => x.CreatedTime, x => x.MapFrom(s => s.CreatedTime))
                .ForMember(x => x.Url, x => x.MapFrom(s => GetUrl(s)))
                .ReverseMap();

            CreateMap<vm.FileData, FileInfo>()
                .ForMember(x => x.FullName, x => x.MapFrom(s => s.FileName))
                .ReverseMap();
        }

        /// <summary> Url для загрузки файла </summary>
        private string GetUrl(FileInfo fileInfo)
        {
            return string.IsNullOrWhiteSpace(fileInfo?.Path)
                ? null
                : $"{_hostName}{fileInfo.Path}";
        }
    }
}
