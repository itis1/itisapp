﻿using System.Threading.Tasks;
using vm = ITISApp.Domain.Teachers.Models;
using dal = ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Teachers.Services
{
    /// <summary> Сервис для загрузки файлов </summary>
    public interface IFileService
    {
        /// <summary> Загрузка файла </summary>
        /// <param name="file"> Файл </param>
        Task<dal.FileInfo> Upload(vm.FileData file);

        /// <summary> Скачать файл </summary>
        /// <param name="resource"> Путь к файлу </param>
        Task<vm.FileData> Download(string resource);

        /// <summary> Удаление файл </summary>
        /// <param name="resource"> Путь к файлу </param>
        Task<dal.FileInfo> Delete(string resource);

        /// <param name="id"> Идентификтаор файла </param>
        Task<dal.FileInfo> Delete(int id);
    }
}
