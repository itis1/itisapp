﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ITISApp.Domain.Core.Interfaces;
using ITISApp.Domain.Teachers.Models;
using ITISApp.Domain.Teachers.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FileInfo = ITISApp.Domain.Core.Entities.FileInfo;
using vm = ITISApp.Domain.Teachers.Models;

namespace ITISApp.Domain.Teachers.Controllers
{
    [Route("api/[controller]", Name = nameof(FileController)), Produces("application/json")/*, Authorize*/]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        private readonly IMapper _mapper;

        private readonly IDataStore _dataStore;

        public FileController(IFileService fileService, IMapper mapper, IDataStore dataStore)
        {
            _fileService = fileService;
            _mapper = mapper;
            _dataStore = dataStore;
        }
    
        [HttpPost("upload")]
        public async Task<vm.FileInfo> Upload(IFormCollection files)
        {
            var file = files.Files.FirstOrDefault();
            if (file == null) throw new Exception("Пара не найдена.");
            using (var stream = new MemoryStream())
            {
                await file.OpenReadStream().CopyToAsync(stream);

                var uploadedFile = await _fileService.Upload(new FileData
                {
                    //Lesson = lesson,
                    ContentType = file.ContentType,
                    FileName = file.FileName,
                    Data = stream.ToArray()
                });

                var result = _mapper.Map<vm.FileInfo>(uploadedFile);

                return result;
            }
        }

        [HttpGet("{*resource}")]
        public async Task<FileResult> Get([FromRoute] string resource)
        {
            var res = await _fileService.Download(resource);
            Response.Headers.Add("fileName", res.FileName);
            return File(res.Data, res.ContentType);
        }

        [HttpGet("options/getAll")]
        public async Task<List<vm.FileInfo>> GetAllFiles()
        {
            var fileInfos = await _dataStore.GetAll<FileInfo>().ToListAsync();

            return _mapper.Map<List<vm.FileInfo>>(fileInfos);
        }

        [HttpDelete("{*resource}")]
        public async Task<vm.FileInfo> Delete([FromRoute] string resource)
        {
            var res = await _fileService.Delete(resource);

            return _mapper.Map<vm.FileInfo>(res);
        }

        [HttpDelete("{id}")]
        public async Task<vm.FileInfo> Delete([FromRoute] int id)
        {
            var res = await _fileService.Delete(id);

            return _mapper.Map<vm.FileInfo>(res);
        }
    }
}
