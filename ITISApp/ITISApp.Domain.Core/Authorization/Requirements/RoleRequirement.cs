﻿using Microsoft.AspNetCore.Authorization;

namespace ITISApp.Domain.Core.Authorization.Requirements
{
    public class RoleRequirement: IAuthorizationRequirement
    {
        public string Code { get; }

        public RoleRequirement(string roleCode)
        {
            Code = roleCode;
        }   
    }
}