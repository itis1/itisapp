﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Authorization.Requirements;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace ITISApp.Domain.Core.Authorization.Handlers
{
    public class PermissionHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly IDataStore _dataStore;

        public PermissionHandler(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionRequirement requirement)
        {
            var roleCodes = context.User.Claims
                .Where(x => x.Type == ClaimTypes.Role)
                .Select(x => x.Value);

            var roleIds = _dataStore.GetAll<Role>()
                .Where(x => roleCodes.Contains(x.Code))
                .Select(x => x.Id)
                .ToList();

            var permissionCodes = _dataStore.GetAll<RolePermission>()
                .Where(x => roleIds.Contains(x.Role.Id))
                .Select(x => x.Permission.Code)
                .ToList();

            if (permissionCodes.Contains(requirement.Code))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}