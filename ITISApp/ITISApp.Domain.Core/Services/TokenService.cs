﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using ITISApp.Domain.Core.Common;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace ITISApp.Domain.Core.Services
{
    /// <summary>
    /// Service that generate JWT tokens.
    /// </summary>
    public class TokenService
    {
        private readonly IDataStore _dataStore;

        public TokenService(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public JwtSecurityToken GenerateToken(User user)
        {
            var key = JwtOptions.GetSymmetricSecurityKey();
            var now = DateTime.Now;
            var jwt = new JwtSecurityToken(
                JwtOptions.Issuer,
                JwtOptions.Audience,
                GetIdentity(user).Claims,
                now,
                now.AddMinutes(JwtOptions.Lifetime),
                new SigningCredentials(key, SecurityAlgorithms.HmacSha256));
            return jwt;
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Login)
            };

            var claimsWithRoles = _dataStore.GetAll<UserRole>()
                .Where(x => x.User.Id == user.Id)
                .Select(userRole => new Claim(ClaimTypes.Role, userRole.Role.Code))
                .ToList();

            claims.AddRange(claimsWithRoles);

            var claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}