﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common.Exceptions;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Core.Dto;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Extensions;
using ITISApp.Domain.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ITISApp.Domain.Core.Services
{
    public class UserDomainService
    {
        private readonly IDataStore _dataStore;
        private readonly PasswordCheckService _passwordService;
        private readonly TokenService _tokenService;
        private readonly EmailService _emailService;

        public UserDomainService(IDataStore dataStore,
            PasswordCheckService passwordService, TokenService tokenService, EmailService emailService)
        {
            _dataStore = dataStore;
            _passwordService = passwordService;
            _tokenService = tokenService;
            _emailService = emailService;
        }

        public async Task<OperationResult> CheckLoginUsed(string login)
        {
            var user = await _dataStore.GetAll<User>()
                .FirstOrDefaultAsync(x => x.Login == login);

            return new OperationResult(user == null, "Данный логин уже используется");
        }

        public async Task<OperationResult> ForgotPassword(string login, string host)
        {
            var user = await _dataStore.GetAll<User>()
                .FirstOrDefaultAsync(x => x.Login == login);

            var guid = Guid.NewGuid().ToString();

            var checkExistToken =
                await _dataStore.GetAll<ResetPasswordToken>().FirstOrDefaultAsync(x => x.User.Id == user.Id);

            if (checkExistToken != null)
            {
                checkExistToken.Token = guid;
                await _dataStore.UpdateAndSaveAsync(checkExistToken);
            }
            else
            {
                var resetPasswordToken = new ResetPasswordToken
                {
                    Token = guid,
                    User = user
                };
                
                await _dataStore.CreateAndSaveAsync(resetPasswordToken);
            }
            
            var messageBody = _emailService.GetForgotPasswordMessage(host, guid);

            await _emailService.SendEmailAsync(user.Email, "Восстановление пароля", messageBody);

            return new OperationResult
            {
                Success = true,
                Data = user?.Email
            };
        }
        
        public async Task<OperationResult> ResetPassword(string token, string newPassword)
        {
            var resetPasswordToken = await _dataStore.GetAll<ResetPasswordToken>().FirstOrDefaultAsync(x => x.Token == token);

            var user = resetPasswordToken?.User;

            if (user == null)
            {
                return new OperationResult(false, "Произошла ошибка при сбросе пароля, попробуйте заново восстановить пароль");
            }

            user.Password = _passwordService.HashPassword(newPassword);
            await _dataStore.UpdateAndSaveAsync(user);

            await _dataStore.DeleteAndSaveAsync(resetPasswordToken);

            return new OperationResult(true);
        }
        
        public async Task<OperationResult> CheckEmailUsed(string email)
        {
            var user = await _dataStore.GetAll<User>()
                .FirstOrDefaultAsync(x => x.Email == email);

            return new OperationResult(user == null, "Данный Email уже используется");
        }

        public async Task<OperationResult> LogIn(LoginDto dto)
        {
            var user = await _dataStore.GetAll<User>()
                           .FirstOrDefaultAsync(x => x.Login == dto.Login)
                       ?? throw new AuthorizationException(
                           "Пользователь с таким логином или Email не существует. Проверьте корректность введенных данных.",
                           nameof(dto.Login));

            if (!_passwordService.Check(user.Password, dto.Password))
            {
                throw new AuthorizationException("Неправильный пароль. Проверьте корректность введенных данных.",
                    nameof(dto.Password));
            }

            var currentSessions = await _dataStore.GetAll<UserSession>()
                .Where(x => x.User.Id == user.Id)
                .ToListAsync();
            if (currentSessions.Count >= 5)
            {
                foreach (var currentSession in currentSessions)
                {
                    await _dataStore.DeleteAndSaveAsync(currentSession);
                }
            }

            var session = new UserSession
            {
                User = user,
                CreatedAt = DateTime.Now,
                ExpiresAt = DateTime.Now.AddMonths(1),
                RefreshToken = Guid.NewGuid(),
                Ip = dto.Ip,
                FingerPrint = dto.FingerPrint
            };
            session = await _dataStore.CreateAndSaveAsync(session);
            var result = new AuthorizationResult
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(_tokenService.GenerateToken(user)),
                RefreshToken = session.RefreshToken
            };

            return new OperationResult
            {
                Success = true,
                Data = result
            };
        }

        public async Task<OperationResult> Register(LoginDto dto)
        {
            var user = new User
            {
                Login = dto.Login,
                Password = _passwordService.HashPassword(dto.Password),
                Email = dto.Email
            };

            user = await _dataStore.CreateAndSaveAsync(user);
            var session = new UserSession
            {
                User = user,
                CreatedAt = DateTime.Now,
                ExpiresAt = DateTime.Now.AddMonths(1),
                RefreshToken = Guid.NewGuid(),
                Ip = dto.Ip,
                FingerPrint = dto.FingerPrint
            };
            session = await _dataStore.CreateAndSaveAsync(session);

            var result = new AuthorizationResult
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(_tokenService.GenerateToken(user)),
                RefreshToken = session.RefreshToken
            };

            return new OperationResult
            {
                Success = true,
                Data = result
            };
        }

        public async Task<OperationResult> RefreshTokens(Guid refreshToken)
        {
            var currentSession = await _dataStore.GetAll<UserSession>()
                                     .FirstOrDefaultAsync(x => x.RefreshToken == refreshToken)
                                 ?? throw new AuthorizationException(
                                     "Сессия больше не существует.");
            var user = currentSession.User;
            var ip = currentSession.Ip;
            var fingerPrint = currentSession.FingerPrint;
            await _dataStore.DeleteAndSaveAsync(currentSession);
            var session = new UserSession
            {
                User = user,
                CreatedAt = DateTime.Now,
                ExpiresAt = DateTime.Now.AddMonths(1),
                RefreshToken = Guid.NewGuid(),
                Ip = ip,
                FingerPrint = fingerPrint
            };
            session = await _dataStore.CreateAndSaveAsync(session);
            var result = new AuthorizationResult
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(_tokenService.GenerateToken(user)),
                RefreshToken = session.RefreshToken
            };

            return new OperationResult
            {
                Success = true,
                Data = result
            };
        }

        public async Task<OperationResult> GetSessionUser(Guid refreshToken, ClaimsPrincipal claimsPrincipal)
        {
            var currentSession = await _dataStore.GetAll<UserSession>()
                                     .FirstOrDefaultAsync(x => x.RefreshToken == refreshToken)
                                 ?? throw new AuthorizationException(
                                     "Сессия больше не существует.");

            var user = currentSession.User;
            var permissionCodes = await GetPermissionsAsync(user);
            if (claimsPrincipal.IsStudent())
            {
                var student = _dataStore.GetAll<Student>().FirstOrDefault(x => x.User.Id == user.Id);
                return new OperationResult
                {
                    Success = true,
                    Data = new StudentDto(student, user, permissionCodes)
                };
            }

            if (claimsPrincipal.IsTeacher())
            {
                var teacher = _dataStore.GetAll<Teacher>().FirstOrDefault(x => x.User.Id == user.Id);
                return new OperationResult
                {
                    Success = true,
                    Data = new TeacherDto(teacher, user, permissionCodes)
                };
            }

            return new OperationResult
            {
                Success = true,
                Data = user.CreateDto(permissionCodes)
            };
        }

        public async Task<OperationResult> LogOut(Guid refreshToken)
        {
            var currentSession = await _dataStore.GetAll<UserSession>()
                                     .FirstOrDefaultAsync(x => x.RefreshToken == refreshToken)
                                 ?? throw new AuthorizationException(
                                     "Сессия больше не существует.");

            await _dataStore.DeleteAndSaveAsync(currentSession);

            return new OperationResult
            {
                Success = true,
                Message = "Сессия была удалена"
            };
        }

        private async Task<List<string>> GetPermissionsAsync(User user)
        {
            var roleIds = await _dataStore.GetAll<UserRole>()
                .Where(x => x.User.Id == user.Id)
                .Select(x => x.Role.Id)
                .ToListAsync();
            var permissionCodes = await _dataStore.GetAll<RolePermission>()
                .Where(x => roleIds.Contains(x.Role.Id))
                .Select(x => x.Permission.Code)
                .ToListAsync();

            return permissionCodes;
        }
    }
}