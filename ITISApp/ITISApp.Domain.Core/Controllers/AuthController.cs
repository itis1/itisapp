﻿using System;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Common.Exceptions;
using ITISApp.Domain.Core.Common.Results;
using ITISApp.Domain.Core.Dto;
using ITISApp.Domain.Core.Filters;
using ITISApp.Domain.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace ITISApp.Domain.Core.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AuthController : Controller
    {
        private readonly UserDomainService _userDomainService;

        public AuthController(UserDomainService userDomainService)
        {
            _userDomainService = userDomainService;
        }

        [HttpGet]
        public async Task<OperationResult> CheckLoginUsed(string login)
        {
            return await _userDomainService.CheckLoginUsed(login);
        }
        
        [HttpGet]
        public async Task<OperationResult> CheckEmailUsed(string email)
        {
            return await _userDomainService.CheckEmailUsed(email);
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginDto dto)
        {
            dto.Ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            OperationResult result;
            try
            {
                result = await _userDomainService.LogIn(dto);
            }
            catch (AuthorizationException exception)
            {
                return NotFound(new
                {
                    exception.Message,
                    exception.ParameterName
                });
            }

            return result.Success ? (IActionResult)Ok(result) : Conflict(result);
        }

        [HttpPost]
        public async Task<OperationResult> Register([FromBody] LoginDto dto)
        {
            var loginCheck = await _userDomainService.CheckLoginUsed(dto.Login);
            if (!loginCheck.Success)
            {
                return loginCheck;
            }

            var emailCheck = await _userDomainService.CheckEmailUsed(dto.Email);
            if (!emailCheck.Success)
            {
                return emailCheck;
            }
            
            dto.Ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            return await _userDomainService.Register(dto);
        }
        
        [HttpPost]
        public async Task<OperationResult> ForgotPassword(string login)
        {
            var domain = Request.Host.Value;
            return await _userDomainService.ForgotPassword(login, domain);
        }
        
        [HttpPost]
        public async Task<OperationResult> ResetPassword(string token, string newPassword)
        {
            return await _userDomainService.ResetPassword(token, newPassword);
        }

        [HttpGet]
        [AuthorizationExceptionFilter]
        public async Task<OperationResult> Refresh(Guid refreshToken)
        {
            return await _userDomainService.RefreshTokens(refreshToken);
        }

        [HttpGet]
        [AuthorizationExceptionFilter]
        public async Task<OperationResult> GetSessionUser(Guid refreshToken)
        {
            return await _userDomainService.GetSessionUser(refreshToken, HttpContext.User);
        }

        [HttpGet]
        [AuthorizationExceptionFilter]
        public async Task<OperationResult> LogOut(Guid refreshToken)
        {
            return await _userDomainService.LogOut(refreshToken);
        }
    }
}