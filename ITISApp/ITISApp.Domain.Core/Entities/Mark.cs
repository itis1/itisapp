﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Отметка о получении баллов.
    /// </summary>
    public class Mark : BaseEntity
    {
        /// <summary>
        /// Количество баллов.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Описание (за что именно получены баллы).
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Занятие, на котором студент получил баллы.
        /// </summary>
        public virtual Lesson Lesson { get; set; }

        /// <summary>
        /// Ссылка на студента.
        /// </summary>
        public virtual Student Student { get; set; }
    }
}