﻿using System;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.Authorization
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public Role()
        {
        }

        public Role(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Role role)
            {
                return role.Code == Code &&
                       role.Name == Name;
            }

            return false;
        }

        protected bool Equals(Role other)
        {
            return Name == other.Name && Code == other.Code;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Code);
        }
    }
}