﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.Authorization
{
    public class ResetPasswordToken : BaseEntity
    {
        public virtual User User { get; set; }

        public virtual string Token { get; set; }
    }
}