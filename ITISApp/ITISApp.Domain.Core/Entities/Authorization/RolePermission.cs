﻿// ReSharper disable VirtualMemberCallInConstructor
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.Authorization
{
    public class RolePermission : BaseEntity
    {
        public virtual Role Role { get; set; }

        public virtual Permission Permission { get; set; }

        public RolePermission()
        {
        }

        public RolePermission(Role role, Permission permission)
        {
            Role = role;
            Permission = permission;
        }
    }
}