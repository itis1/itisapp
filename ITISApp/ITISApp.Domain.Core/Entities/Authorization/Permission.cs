﻿using System;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.Authorization
{
    public class Permission : BaseEntity
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public Permission()
        {
        }

        public Permission(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Permission permission)
            {
                return permission.Code == Code &&
                       permission.Name == Name;
            }

            return false;
        }

        protected bool Equals(Permission other)
        {
            return Name == other.Name && Code == other.Code;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Code);
        }
    }
}