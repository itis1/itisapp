﻿using System;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Сессия авторизации пользователя.
    /// </summary>
    public class UserSession : BaseEntity
    {
        /// <summary>
        /// Ссылка на пользователя.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Токен, для получения нового AccessToken по данным о сессии.
        /// </summary>
        public Guid RefreshToken { get; set; }

        /// <summary>
        /// Время создания сессии.
        /// </summary>
        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// Время, когда сессия истекает.
        /// </summary>
        public DateTime ExpiresAt { get; set; }

        /// <summary>
        /// Ip-адрес, с которого происходила авторизация.
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Идентификатор браузера.
        /// </summary>
        public string FingerPrint { get; set; }
    }
}