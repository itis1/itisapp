﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    public class Teacher : BaseEntity
    {
        /// <summary>
        /// Стаж работы (в годах).
        /// </summary>
        public int WorkingExperience { get; set; }

        /// <summary>
        /// Фамилия учителя и инициалы.
        /// </summary>
        public string TeacherName { get; set; }

        public virtual User User { get; set; }
    }
}