﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Группа студентов.
    /// </summary>
    public class AcademicGroup : BaseEntity
    {
        /// <summary>
        /// Название (номер) группы.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер курса.
        /// </summary>
        public int CourseNumber { get; set; }
    }
}