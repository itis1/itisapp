using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities.LinkEntities
{
    public class SubjectTeacher : BaseEntity
    {
        public virtual Subject Subject { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}