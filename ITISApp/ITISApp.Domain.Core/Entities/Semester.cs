﻿using System;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    public class Semester : BaseEntity
    {
        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }
    }
}