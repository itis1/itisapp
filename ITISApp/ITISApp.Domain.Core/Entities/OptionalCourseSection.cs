﻿using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Entities
{
    /// <summary>
    /// Блок курсов по выбору.
    /// </summary>
    public class OptionalCourseSection : BaseEntity
    {
        /// <summary>
        /// Наименование блока(гуманитарный, научный, практический).
        /// </summary>
        public string Name { get; set; }

        public int CourseNumber { get; set; }
    }
}