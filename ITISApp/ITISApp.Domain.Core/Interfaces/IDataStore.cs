﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ITISApp.Domain.Core.Entities.Base;

namespace ITISApp.Domain.Core.Interfaces
{
    public interface IDataStore
    {
        TEntity Get<TEntity>(int entityId)
            where TEntity : BaseEntity;

        Task<TEntity> GetAsync<TEntity>(int entityId)
            where TEntity : BaseEntity;

        IQueryable<TEntity> GetAll<TEntity>()
            where TEntity : BaseEntity;

        TEntity Create<TEntity>(TEntity entity)
            where TEntity : BaseEntity;
        
        TEntity CreateAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        Task<TEntity> CreateAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity;
        
        Task<TEntity> CreateAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        TEntity Update<TEntity>(TEntity entity)
            where TEntity : BaseEntity;
        
        TEntity UpdateAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        Task<TEntity> UpdateAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        void Delete<TEntity>(TEntity entity)
            where TEntity : BaseEntity;
        
        void DeleteAndSave<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        Task DeleteAndSaveAsync<TEntity>(TEntity entity)
            where TEntity : BaseEntity;

        void SaveChanges();

        Task SaveChangesAsync();

        void InTransaction(Action action);

        Task InTransactionAsync(Func<Task> action);
    }
}