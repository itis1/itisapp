﻿using System;
using System.Collections.Generic;
using System.Text;
using ITISApp.Domain.Core.Entities;

namespace ITISApp.Domain.Core.Interfaces
{
    public interface IHaveSemester
    {
        Semester Semester { get; set; }
    }
}
