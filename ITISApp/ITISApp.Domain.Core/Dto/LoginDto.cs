﻿namespace ITISApp.Domain.Core.Dto
{
    public class LoginDto
    {
        public string Login { get; set; }

        public string Email { get; set; }
        
        public string Password { get; set; }
        public string Ip { get; set; }
        public string FingerPrint { get; set; }
    }
}