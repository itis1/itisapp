﻿using System.Collections.Generic;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;

namespace ITISApp.Domain.Core.Dto
{
    public class UserInfoDto
    {
        public UserInfoDto(User user, List<string> permissionCodes)
        {
            Id = user.Id;
            Login = user.Login;
            Name = user.Name;
            Surname = user.Surname;
            MiddleName = user.MiddleName;
            Email = user.Email;
            ImageUrl = user.ImageUrl;
            PermissionCodes = permissionCodes;
        }

        public int Id { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string MiddleName { get; set; }

        public string ImageUrl { get; set; }

        public List<string> PermissionCodes { get; set; }
    }
}