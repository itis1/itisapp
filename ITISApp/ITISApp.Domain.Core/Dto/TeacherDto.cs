﻿using ITISApp.Domain.Core.Entities;
using System.Collections.Generic;

namespace ITISApp.Domain.Core.Dto
{
    public class TeacherDto : UserInfoDto
    {
        public TeacherDto(Teacher teacher, User user, List<string> permissionCodes)
            : base(user, permissionCodes)
        {
            WorkingExperience = teacher.WorkingExperience;
            IsTeacher = true;
            TeacherId = teacher.Id;
        }

        public bool IsTeacher { get; set; }

        /// <summary>
        /// Стаж работы (в годах).
        /// </summary>
        public int WorkingExperience { get; set; }

        public int TeacherId { get; set; }
    }
}