﻿using System;

namespace ITISApp.Domain.Core.Common.Results
{
    public class AuthorizationResult
    {
        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
    }
}