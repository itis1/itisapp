﻿using System;

namespace ITISApp.Domain.Core.Common.Exceptions
{
    public class AuthorizationException : Exception
    {
        public string ParameterName { get; set; }

        public AuthorizationException(string message, string parameterName = null) 
            : base (message)
        {
            ParameterName = parameterName;
        }
    }
}