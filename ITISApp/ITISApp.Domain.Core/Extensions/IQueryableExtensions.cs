﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using ITISApp.Domain.Core.Interfaces;

namespace ITISApp.Domain.Core.Extensions
{
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Фильтрация по актуальной дате семестра.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="actualDate"></param>
        /// <returns></returns>
        public static IQueryable<T> FilterByActualDate<T>(this IQueryable<T> query, DateTime? actualDate = null)
            where T : IHaveSemester
        {
            actualDate ??= DateTime.Now;

            return query
                .Where(x => x.Semester.DateStart.Date >= actualDate.Value.Date)
                .Where(x => x.Semester.DateEnd.Date >= actualDate.Value.Date);
        }

        /// <summary>
        /// Фильтрация по определенному свойству.
        /// Пример: FilterByProperty(p => p.TimeTableInfo, x => x.FilterByActualDate(DateTime.Now)).
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="query"></param>
        /// <param name="propertySelectExpression"></param>
        /// <param name="propertyQueryFilterFunc"></param>
        /// <returns></returns>
        public static IQueryable<TEntity> FilterByProperty<TEntity, TProperty>(
            this IQueryable<TEntity> query,
            Expression<Func<TEntity, TProperty>> propertySelectExpression,
            Func<IQueryable<TProperty>, IQueryable<TProperty>> propertyQueryFilterFunc)
        {
            IQueryable<TProperty> propertyQuery = query.Select(propertySelectExpression);

            IQueryable<TProperty> filteredPropertyQuery = propertyQueryFilterFunc(propertyQuery);

            var methodDefinition = typeof(Queryable)
                .GetMethods()
                .Where(x => x.Name == "Contains")
                .Single(x => x.GetParameters().Length == 2);

            var method = methodDefinition.MakeGenericMethod(typeof(TProperty));

            var containsExpr = Expression.Call(
                method,
                new[] { filteredPropertyQuery.Expression, propertySelectExpression.Body });

            var lambdaExpr = Expression.Lambda<Func<TEntity, bool>>(
                containsExpr,
                propertySelectExpression.Parameters[0]);

            return query.Where(lambdaExpr);
        }
    }
}
