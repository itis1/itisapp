﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ITISApp.Domain.Core.Dto;
using ITISApp.Domain.Core.Entities;
using ITISApp.Domain.Core.Entities.Authorization;
using ITISApp.Domain.Core.Helpers;

namespace ITISApp.Domain.Core.Extensions
{
    public static class UserExtensions
    {
        public static UserInfoDto CreateDto(this User user, List<string> permissionCodes)
        {
            return new UserInfoDto(user, permissionCodes);
        }

        public static bool IsAdmin(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims
                .Any(x => x.Type == ClaimTypes.Role && x.Value == RoleCodesHelper.Admin);
        }

        public static bool IsTeacher(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims
                .Any(x => x.Type == ClaimTypes.Role && x.Value == RoleCodesHelper.Teacher);
        }

        public static bool IsStudent(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims
                .Any(x => x.Type == ClaimTypes.Role && x.Value == RoleCodesHelper.Student);
        }
    }
}