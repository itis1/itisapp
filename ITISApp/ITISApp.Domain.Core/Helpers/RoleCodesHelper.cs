﻿namespace ITISApp.Domain.Core.Helpers
{
    public static class RoleCodesHelper
    {
        public const string Admin = "Admin";
        public const string Student = "Student";
        public const string Teacher = "Teacher";
    }
}