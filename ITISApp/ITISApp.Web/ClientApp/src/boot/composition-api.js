import Vue from 'vue';
import hooks from '@u3u/vue-hooks';
import VueCompositionApi from '@vue/composition-api';

Vue.use(hooks);
Vue.use(VueCompositionApi);
