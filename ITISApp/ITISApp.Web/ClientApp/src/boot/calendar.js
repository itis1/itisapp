import Vue from 'vue';
import VuePlugin from '@/calendar/index.esm';
import '@/calendar/index.css';

Vue.use(VuePlugin);
