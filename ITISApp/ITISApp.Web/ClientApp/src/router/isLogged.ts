import { store } from '@/store';
import { computed } from '@vue/composition-api';

export default (to: any, from: any, next: any) => {
  const isAuthenticated = computed(() => store.getters['Auth/IsAuthenticated']);
  store
    .dispatch('Auth/authenticateWithToken')
    .then(() => {
      if (isAuthenticated.value) {
        next('/');
      } else {
        next();
      }
    })
    .catch(() => {
      next();
    });
};
