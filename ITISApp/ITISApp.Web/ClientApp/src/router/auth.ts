import { store } from '@/store';
import { computed } from '@vue/composition-api';

const checkAuthenticated = (user: any, to: any, next: any) => {
  if (user.isStudent || user.isTeacher) {
    next();
  } else {
    if (to.fullPath == '/select') {
      next();
    } else {
      next('/select');
    }
  }
};

export default (to: any, from: any, next: any) => {
  const isAuthenticated = computed(() => store.getters['Auth/IsAuthenticated']);
  const user = computed(() => store.getters['Auth/User']);
  if (isAuthenticated.value) {
    checkAuthenticated(user.value, to, next);
  } else {
    store
      .dispatch('Auth/authenticateWithToken')
      .then(() => {
        if (isAuthenticated.value) {
          checkAuthenticated(user.value, to, next);
        } else {
          next('/login');
        }
      })
      .catch(() => {
        next('/login');
      });
  }
};
