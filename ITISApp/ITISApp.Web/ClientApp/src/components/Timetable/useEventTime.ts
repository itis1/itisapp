import Lesson from '@/models/Lesson';
//@ts-ignore
import QCalendar from '@quasar/quasar-ui-qcalendar';

export function useEventTime() {
  const getEndTime = (lesson: Lesson) => {
    let endTime = QCalendar.parseTimestamp(lesson.date + ' ' + lesson.time);
    endTime = QCalendar.addToDate(endTime, { minute: lesson.duration });
    endTime = QCalendar.getTime(endTime);
    return endTime;
  };

  return {
    getEndTime,
  };
}
