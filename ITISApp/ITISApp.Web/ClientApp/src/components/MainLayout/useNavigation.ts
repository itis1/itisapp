import { onMounted, ref, computed } from '@vue/composition-api';

export function useNavigation() {
  onMounted(() => {
    let container = document.getElementsByClassName('drawer')[0];
    let drawer = container?.children[0] as HTMLElement;
    drawer.style.marginLeft = '64px';
  });

  const drawer = ref(false);
  const miniState = ref(true);
  const fixed = ref(false);

  const isMini = computed(() => {
    if (fixed.value) return false;
    return miniState.value;
  });

  const fix = () => {
    fixed.value = !fixed.value;
  };

  const mouseOver = () => {
    document.getElementById('expand_btn')!.style.opacity = '1';
    miniState.value = false;
  };

  const mouseOut = (e: MouseEvent) => {
    if (fixed.value) document.getElementById('expand_btn')!.style.opacity = '0';
    if ((e?.relatedTarget as HTMLElement)?.offsetParent?.id == 'expand_btn' || (e?.relatedTarget as HTMLElement)?.id == 'expand_btn') return;
    miniState.value = true;
  };

  const mouseOverBtn = (e: MouseEvent) => {
    document.getElementById('expand_btn')!.style.opacity = '1';
    e.stopPropagation();
  };

  return {
    drawer,
    miniState,
    isMini,
    mouseOver,
    mouseOut,
    fix,
    fixed,
    mouseOverBtn,
  };
}
