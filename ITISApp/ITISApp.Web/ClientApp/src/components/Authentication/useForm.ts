import { ref } from '@vue/composition-api';
import { useRouter } from '@u3u/vue-hooks';
import { Loading } from 'quasar';
// @ts-ignore
import { required, minLength, email as emailValidator } from '@vuelidate/validators';
import useVuelidate from '@/boot/vuelidate/index';
import { loginExist } from '@/utils/loginExistValidator';
import { emailExist } from '@/utils/emailExistValidator';

import { triggerPositive, triggerNegative } from '@/utils/Notifications';

export function useForm(action: (...args: any[]) => Promise<any>, type: 'login' | 'registration') {
  const { router } = useRouter();

  const login = ref('');
  const email = ref('');
  const password = ref('');

  const loginRules = {
    login: { required },
    password: { required },
  };

  const registrationRules = {
    login: { required, loginExist, $autoDirty: true },
    password: { required, minLength: minLength(8), $autoDirty: true },
    email: { required, emailExist, emailValidator, $autoDirty: true },
  };
  // @ts-ignore
  const $v = useVuelidate(type === 'login' ? loginRules : registrationRules, { login, password, email });

  const clearForm = (parameterName: string) => {
    $v.$reset();
    password.value = '';
    email.value = '';
    if (parameterName != 'Password') {
      login.value = '';
    }
  };

  const submit = async () => {
    $v.$touch();
    if (!$v.$invalid) {
      const response = await action({ login: login.value, password: password.value, email: email.value });
      if (response.success) {
        triggerPositive('Добро пожаловать');
        Loading.hide();
        if (type == 'login') {
          router.push('/');
        } else if (type == 'registration') {
          router.push('/select');
        }
      } else {
        triggerNegative(response.message);
        Loading.hide();
      }
      clearForm(response.parameterName);
    }
  };

  return { login, password, email, submit, $v };
}
