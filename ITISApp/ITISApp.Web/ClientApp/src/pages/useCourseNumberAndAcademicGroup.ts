import { useActions, useGetters } from '@u3u/vue-hooks';
import { onMounted, ref } from '@vue/composition-api';
import AcademicGroup from '@/models/AcademicGroup';

export function useCourseNumberAndAcademicGroup() {
  const { User: user } = useGetters('Auth', ['User']);
  const { getAcademicGroups } = useActions('Profile', ['getAcademicGroups']);
  const { AcademicGroups: academicGroups } = useGetters('Profile', ['AcademicGroups']);

  const courseNumber = ref(user.value.courseNumber);
  const academicGroup = ref(user.value.academicGroup);

  const handleChangeCourse = async (value: string) => {
    courseNumber.value = parseInt(value);
    await getAcademicGroups(courseNumber.value);
    academicGroup.value = academicGroups.value[0];
  };

  const handleAcademicGroupChange = async (value: AcademicGroup) => {
    academicGroup.value = value;
  };

  onMounted(async () => {
    if (user.value.isStudent) {
      await getAcademicGroups(courseNumber.value);
    }
  });

  return {
    user,
    academicGroup,
    courseNumber,
    handleChangeCourse,
    handleAcademicGroupChange,
    getAcademicGroups,
  };
}
