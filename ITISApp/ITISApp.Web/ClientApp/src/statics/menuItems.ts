export const menuItems = [
  {
    title: 'Расписание',
    icon: 'calendar_today',
    link: '/timetable',
  },
  {
    title: 'Успеваемость',
    icon: 'insert_chart',
    link: '/performance',
  },
];
