import { Getters, Mutations, Actions, Module } from 'vuex-smart-module';
import { api } from '@/store/api/api';
import Lecture from '@/models/Lecture';
import store from '..';
class LecturesState {
  lectures: Lecture[] = [];
}

class LecturesGetters extends Getters<LecturesState> {
  get allLectures() {
    return this.state.lectures;
  }
}

class LecturesMutations extends Mutations<LecturesState> {
  setLectures(lectures: Lecture[]) {
    this.state.lectures = lectures;
  }
}

class LecturesActions extends Actions<LecturesState, LecturesGetters, LecturesMutations, LecturesActions> {
  async getLectures() {
    const result = await api.client.get('/api/file/options/getAll');

    if (result.status === 200) {
      this.commit('setLectures', result.data);
    }
  }
  
  async downloadLecture(path: string) {
    const result = await api.client.get('/api/file/' + path);
    var downloadElement = document.createElement('a');
    downloadElement.href = '/api/file/' + path;
    downloadElement.download = result.headers.filename;
    document.body.appendChild(downloadElement);
    downloadElement.click();
    document.body.removeChild(downloadElement);
    window.URL.revokeObjectURL(downloadElement.href);
  }

  async removeLecture(path: string) {
    const result = await api.client.delete('/api/file/' + path);
    this.dispatch("getLectures");
  }
}

export const Lectures = new Module({
  state: LecturesState,
  getters: LecturesGetters,
  mutations: LecturesMutations,
  actions: LecturesActions
});
