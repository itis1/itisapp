import { Module } from 'vuex-smart-module';
import { Auth } from '@/store/modules/Auth';
import { Lectures } from '@/store/modules/Lectures';
import { Timetable } from '@/store/modules/Timetable';
import { Profile } from '@/store/modules/Profile';
import { OptionalGroup } from '@/store/modules/OptionalGroup';

export const Root = new Module({
  modules: {
    Auth,
    Timetable,
    Profile,
    Lectures,
    OptionalGroup,
  },
});
