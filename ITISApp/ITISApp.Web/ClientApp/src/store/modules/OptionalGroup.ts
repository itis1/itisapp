import { Getters, Mutations, Actions, Module } from 'vuex-smart-module';
import { api } from '@/store/api/api';
import OptionalSection from '@/models/OptionalSection';
import Student from '@/models/Student';
import Subject from '@/models/Subject';
import SubjectTeacher from '@/models/SubjectTeacher';
import SubGroup from '@/models/SubGroup';

class OptionalGroupState {
  optionalSections: OptionalSection[] = [];
  subjects: Subject[] = [];
  teachers: SubjectTeacher[] = [];
  subGroups: SubGroup[] = [];
}

class OptionalGroupGetters extends Getters<OptionalGroupState> {
  get OptionalSections() {
    return this.state.optionalSections;
  }
  get Subjects() {
    return this.state.subjects;
  }
  get Teachers() {
    return this.state.teachers;
  }
  get SubGroups() {
    return this.state.subGroups;
  }
}

class OptionalGroupMutations extends Mutations<OptionalGroupState> {
  setOptionalSections(optionalSections: OptionalSection[]) {
    this.state.optionalSections = optionalSections;
  }
  setSubjects(subjects: Subject[]) {
    this.state.subjects = subjects;
  }
  setTeachers(teachers: SubjectTeacher[]) {
    this.state.teachers = teachers;
  }
  clearTeachers() {
    this.state.teachers = [];
  }
  setSubGroups(subGroups: SubGroup[]) {
    this.state.subGroups = subGroups;
  }
  clearSubGroups() {
    this.state.subGroups = [];
  }
}

class OptionalGroupActions extends Actions<OptionalGroupState, OptionalGroupGetters, OptionalGroupMutations, OptionalGroupActions> {
  async getOptionalSections(student: Student) {
    const result = await api.getOptionalSections(student.studentId!, student.courseNumber);

    if (result.success) {
      this.commit('setOptionalSections', result.data);
    }
  }

  async clearTeachers() {
    this.commit('clearTeachers');
  }

  async clearSubGroups() {
    this.commit('clearSubGroups');
  }

  async getSubjects(sectionId: number) {
    const result = await api.getSubjects(sectionId);

    if (result.success) {
      this.commit('setSubjects', result.data);
    }
  }
  async getTeachers(subjectId: number) {
    const result = await api.getTeachers(subjectId);

    if (result.success) {
      this.commit('setTeachers', result.data);
    }
  }

  async getSubGroups(payload: { teacherId: number; academicGroupId: number }) {
    const result = await api.getSubGroups(payload.teacherId, payload.academicGroupId);

    if (result.success) {
      this.commit('setSubGroups', result.data);
    }
  }

  async addStudentToOptionalGroup(payload: { studentId: number; subGroup: number; section: OptionalSection }) {
    const result = await api.addStudentToOptionalGroup(payload.studentId, payload.subGroup, payload.section);

    if (result.success) {
      this.commit('setSubGroups', result.data);
    }
  }
}

export const OptionalGroup = new Module({
  state: OptionalGroupState,
  getters: OptionalGroupGetters,
  mutations: OptionalGroupMutations,
  actions: OptionalGroupActions,
});
