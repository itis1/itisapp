import { Actions, Getters, Module, Mutations } from 'vuex-smart-module';
import { api } from '@/store/api/api';
import User from '@/models/User';
import Fingerprint from '@/utils/GetFingerprint';

class AuthState {
  user: User | null = null;
}

class AuthGetters extends Getters<AuthState> {
  get User() {
    return this.state.user;
  }

  get IsAuthenticated() {
    return this.state.user != null;
  }
}

class AuthMutations extends Mutations<AuthState> {
  setUser(user: User | null) {
    this.state.user = user;
  }
}

class AuthActions extends Actions<AuthState, AuthGetters, AuthMutations, AuthActions> {
  async login({ login, password }: { login: string; password: string }) {
    let fingerprint = (await Fingerprint()) as string;
    const result = await api.login({ login, password, fingerprint });
    if (result.success) {
      let user = (await api.getSessionUser()) as User;
      this.commit('setUser', user);
    }
    return result;
  }

  async register({ login, password, email }: { login: string; password: string; email: string }) {
    let fingerprint = (await Fingerprint()) as string;
    const result = await api.register({ login, password, email, fingerprint });
    if (result.success) {
      let user = (await api.getSessionUser()) as User;
      this.commit('setUser', user);
    }
    return result;
  }

  async authenticateWithToken() {
    await api.authenticateWithToken();
    let user = (await api.getSessionUser()) as User;
    this.commit('setUser', user);
  }

  async checkLoginUsed(login: string) {
    const response = await api.checkLoginUsed(login);
    return response == true;
  }

  async forgotPassword(login: string) {
    return await api.forgotPassword(login);
  }

  async resetPassword({ token, newPassword }: { token: string; newPassword: string }) {
    return await api.resetPassword(token, newPassword);
  }

  async checkEmailUsed(email: string) {
    const response = await api.checkEmailUsed(email);
    return response == true;
  }

  async logout() {
    await api.logout();
    this.commit('setUser', null);
  }
}

export const Auth = new Module({
  state: AuthState,
  getters: AuthGetters,
  mutations: AuthMutations,
  actions: AuthActions,
});
