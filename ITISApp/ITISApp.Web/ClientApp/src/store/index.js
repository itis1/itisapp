import Vue from 'vue';
import Vuex from 'vuex';
import { createStore } from 'vuex-smart-module';
import { Root } from './modules/Root';

Vue.use(Vuex);

const Store = createStore(Root, {
  strict: process.env.DEV
});

export default function() {
  return Store;
}

export const store = Store;
