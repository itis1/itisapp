import { Notify } from 'quasar';

export function triggerPositive(message: string, timeOut?: number) {
  Notify.create({
    type: 'positive',
    message: message,
    timeout: timeOut || 2000,
    progress: true,
  });
}

export function triggerNegative(message: string, timeOut?: number) {
  Notify.create({
    type: 'negative',
    message: message,
    timeout: timeOut || 2000,
    progress: true,
  });
}

export function triggerWarning(message: string) {
  Notify.create({
    type: 'warning',
    message: message,
    timeout: 2000,
    progress: true,
  });
}
