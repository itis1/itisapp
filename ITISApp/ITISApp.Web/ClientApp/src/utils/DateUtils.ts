import { format, parse } from 'date-fns';
import { ru } from 'date-fns/locale';

export function formatDate(sourceDate: Date, formatString: string) {
  return format(sourceDate, formatString, { locale: ru });
}
export function parseDate(sourceDate: string, formatString: string) {
  return parse(sourceDate, formatString, new Date(), { locale: ru });
}
