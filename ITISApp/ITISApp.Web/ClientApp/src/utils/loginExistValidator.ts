import { useActions } from '@u3u/vue-hooks';

const { checkLoginUsed } = useActions('Auth', ['checkLoginUsed']);

const loginExistValidator = async (login: string) => {
  if (login) return await checkLoginUsed(login);
  return false;
};

export const loginExist = {
  $validator: loginExistValidator,
  $message: 'Данный логин уже используется',
};
