export default class Lecture {
  fullName: string;
  name: string;
  extension: string;
  contentType: string;
  path: string;

  constructor(fullName: string, name: string, extension: string, contentType: string, path: string) {
    this.fullName = fullName;
    this.name = name;
    this.extension = extension;
    this.contentType = contentType;
    this.path = path;
  }
}
