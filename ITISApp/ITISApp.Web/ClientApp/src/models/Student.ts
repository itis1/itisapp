import User from '@/models/User';
import AcademicGroup from '@/models/AcademicGroup';

export default class Student extends User {
  academicGroup: AcademicGroup;
  courseNumber: number;
  studentId: number | undefined = undefined;
  isStudent: boolean;

  constructor(
    id: number,
    login: string,
    academicGroup: AcademicGroup,
    courseNumber: number,
    name?: string,
    surname?: string,
    middleName?: string,
    email?: string,
    imageUrl?: string,
    studentId?: number
  ) {
    super(id, login, name, surname, middleName, email, imageUrl);
    this.academicGroup = academicGroup;
    this.courseNumber = courseNumber;
    this.isStudent = true;
    this.studentId = studentId;
  }
}
