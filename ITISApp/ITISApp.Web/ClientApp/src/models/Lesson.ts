export default class Lesson {
  title: string;
  date: string;
  time: string;
  duration: number;
  roomNumber: string;
  teacherName?: string;
  bgcolor: string;
  groups?: string;

  constructor(
    title: string,
    date: string,
    time: string,
    duration: number,
    roomNumber: string,
    bgcolor: string,
    groups?: string,
    teacherName?: string
  ) {
    this.title = title;
    this.date = date;
    this.time = time;
    this.roomNumber = roomNumber;
    this.teacherName = teacherName;
    this.duration = duration;
    this.bgcolor = bgcolor;
    this.groups = groups;
  }
}
