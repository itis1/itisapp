export default class SubGroup {
  id: number;
  subGroupNumber: string;

  constructor(id: number, subGroupNumber: string) {
    this.id = id;
    this.subGroupNumber = subGroupNumber;
  }
}
