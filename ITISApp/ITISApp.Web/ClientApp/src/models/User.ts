export default class User {
  id: number;
  login: string;
  name: string | undefined = '';
  surname: string | undefined = '';
  middleName: string | undefined = '';
  email: string | undefined = '';
  imageUrl: string | undefined = '';

  constructor(id: number, login: string, name?: string, surname?: string, middleName?: string, email?: string, imageUrl?: string) {
    this.id = id;
    this.login = login;
    this.name = name;
    this.surname = surname;
    this.middleName = middleName;
    this.email = email;
    this.imageUrl = imageUrl;
  }
}
