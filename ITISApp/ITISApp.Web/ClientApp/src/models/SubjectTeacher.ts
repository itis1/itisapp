export default class SubjectTeacher {
  id: number;
  teacherName: string;

  constructor(id: number, teacherName: string) {
    this.id = id;
    this.teacherName = teacherName;
  }
}
