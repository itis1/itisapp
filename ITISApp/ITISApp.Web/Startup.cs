using System;
using System.Text;
using AutoMapper;
using ITISApp.DAL;
using ITISApp.Domain.Core.Controllers;
using ITISApp.Domain.Students.Controllers;
using ITISApp.Domain.Teachers.Mapper;
using ITISApp.Domain.Teachers.Models;
using ITISApp.Web.Services.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using VueCliMiddleware;

namespace ITISApp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var coreAssembly = typeof(AuthController).Assembly;
            var studentAssembly = typeof(TimetableController).Assembly;
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            services
                .AddMvcCore()
                .AddApiExplorer()
                .AddApplicationPart(coreAssembly)
                .AddApplicationPart(studentAssembly);

            services.AddDbContext<ApplicationDbContext>(opt =>
                opt
                    .UseLazyLoadingProxies()
                    .UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));


            services.AddScoped<DbContext, ApplicationDbContext>();

            services.Configure<DomainOptions>(Configuration.GetSection(nameof(DomainOptions)));

            services.RegisterServices();
            services.AddJwtAuthentication();
            services.AddPoliciesAuthorization();

            var mappingConfig = new MapperConfiguration(cfg => 
            {
                cfg.AddProfile(
                new MappingProfile(Configuration.GetSection(nameof(DomainOptions)).Get<DomainOptions>()));

            });
            services.AddSingleton(mappingConfig.CreateMapper());

            services.AddSpaStaticFiles(opt => opt.RootPath = "ClientApp/dist/spa");
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "ITISApp API", Version = "v1" });
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    In = ParameterLocation.Header,
                    Description = "Вставьте JWT в следующем формате: Bearer *token*",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new string[] { }
                    }
                });
                options.OperationFilter<FileUploadOperation>();
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider provider)
        {
            provider.CheckRegisteredRolesForExisting();
            provider.CheckRegisteredPermissionsForExisting();
            provider.CheckRegisteredRolePermissionsForExisting();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseSpaStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapToVueCliProxy(
                    "{*path}",
                    new SpaOptions { SourcePath = "ClientApp" },
                    npmScript: (System.Diagnostics.Debugger.IsAttached) ? "serve" : null,
                    forceKill: true);
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ITISApp API V1");
            });
        }
    }
}
